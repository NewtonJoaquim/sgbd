
public class Edge {
	//representa uma possivel operacao
	
	public Node node1;
	public Node node2;
	public operations opr;
	
	public Edge (Node n1, Node n2, operations operation) {
		node1 = n1;
		node2 = n2;
		opr = operation;
	}
}