import java.util.ArrayList;

public class Graph {
	//usado para manter o grafo e desenha-lo?
	
	//Atributos
	ArrayList<Transaction> trList = new ArrayList<Transaction>();
	ArrayList<Node> nodeList = new ArrayList<Node>();
	//ArrayList<Edge> edgeList;
	
	
	public Graph() {
	
		//ArrayList<Transaction> trList = new ArrayList<Transaction>();
		//ArrayList<Node> nodeList = new ArrayList<Node>();
		//criando nos com estados:
		nodeList.add(new Node(estados.INICIADA));
		nodeList.add(new Node(estados.ATIVA)); 
		nodeList.add(new Node(estados.EFETIVACAO)); 
		nodeList.add(new Node(estados.EFETIVADA)); 
		nodeList.add(new Node(estados.CANCELAMENTO)); 
		nodeList.add(new Node(estados.FINALIZADA)); 
		
	}
	
	public void addTR (int count) {
		Transaction newTR = new Transaction(count);
		this.nodeList.get(0).addTR(newTR);
		this.trList.add(newTR);
	}
	
	public void listTR () {
		for (Transaction aux: trList) {
			System.out.println( aux.getTrID());
		}
	}
	
	public Transaction findTR (int id) {
		Transaction aux_tr = null;
		for (Transaction aux: trList) {
			if (aux.getTrID() == id) {
				aux_tr = aux;
				break;
			}
		}
		return aux_tr;
	}

	public Node findNode (estados est) {
		Node aux_node = null;
		for (Node aux: nodeList) {
			if (aux.estadoNode == est) {
				aux_node = aux;
				break;
			}
		}
		return aux_node;
	}
	
	public void mudarEstado (Transaction trc, estados novoEstado) {
		Node atual = this.findNode(trc.getEstadoAtual());
		Node novo = this.findNode(novoEstado);
		novo.addTR(trc);
		atual.rmvTR(trc);
	}
	
	public void consultarEstados () {
		for (Transaction aux: trList) {
			System.out.println(aux.getTrID()+" : "+aux.getEstadoAtual());
		}
	}
}