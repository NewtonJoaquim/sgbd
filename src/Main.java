import java.util.Scanner;

/*
 * grafo: o grafo de estados
 * opt: variavel de opcao do primeiro menu
 * opt2: variavel de opcao do menu de operacoes
 * keyboard: recebe entradas do teclado
 * transaction: auxiliar para manipular transacoes durante operacoes*/

public class Main {

	public static Graph grafo = new Graph();
	public static int opt=0;
	public static Scanner keyboard = new Scanner(System.in);
	public static int tr_count = 0;
	
	public static void main (String[] args) {
		do{
			//PRINTAR MENU
			System.out.println("1: Nova transa��o");
			System.out.println("2: Realizar Opera��o");
			System.out.println("3: Consultar transa��o");
			System.out.println("4: Sair");
		
			opt = keyboard.nextInt();
		
			switch (opt) {
		
				case 1:
						//nova transacao
						grafo.addTR(tr_count);
						//Transaction tr = new Transaction(tr_count);
						//grafo.nodeList.get(0).addTR(tr);
						//System.out.println(grafo.nodeList.size());
						tr_count++;
						System.out.println("Transa��o criada com sucesso!\n Transa��es atuais:\n");
						grafo.listTR();
						System.out.println("");
					break;
					
				case 2:
					int opt2 = 0;
				
					do {
						System.out.println("1: access");
						System.out.println("2: terminate");
						System.out.println("3: rollback");
						System.out.println("4: commit");
						System.out.println("5: finish");
						System.out.println("6: voltar ao menu principal");
						//PRINTAR MENU
						
						opt2 = keyboard.nextInt();
					
						switch (opt2) {
					
							//PRINTAR ESTADO DE TODAS AS transacoes apos CADA operacao
					
								/*case 1:	//tr begin
									//grafo.addTR();		//duas opcoes! adicionar transacao no grafo ou na main, na chamada da operacao?
									Transaction newTR = new Transaction();
									//grafo.nodeList.get(0).addTR(newTR);
									System.out.println("Transa��o criada com sucesso!\n Transa��es atuais:\n");
									grafo.listTR();
									break;*/
								
								case 1:		//tr access
									//selecionando uma transacao para a operacao
									System.out.println("Selecione uma transacao:");
									grafo.listTR();
									int aux2 = keyboard.nextInt();
									Transaction tr2 = grafo.findTR(aux2);
								
									//verificando se a operacao eh possivel nessa transacao
									if (tr2.getEstadoAtual() == estados.INICIADA || tr2.getEstadoAtual() == estados.ATIVA) {
										grafo.mudarEstado(tr2, estados.ATIVA);
										System.out.println("Opera��o realizada com sucesso.");
										
									} else
										System.out.println("Esta operacao nao pode ser efetuada no momento.");
									break;
								
								case 2:		//tr terminate
									//selecionando uma transacao para a operacao
									System.out.println("Selecione uma transacao:");
									grafo.listTR();
									int aux3 = keyboard.nextInt();
									Transaction tr3 = grafo.findTR(aux3);
								
									//verificando se a operacao eh possivel nessa transacao
									if (tr3.getEstadoAtual() == estados.ATIVA) {
										grafo.mudarEstado(tr3, estados.EFETIVACAO);
										System.out.println("Opera��o realizada com sucesso.");
									} else
										System.out.println("Esta operacao nao pode ser efetuada no momento.");
									break;
								
								case 3:		//tr rollback
									//selecionando uma transacao para a operacao
									System.out.println("Selecione uma transacao:");
									grafo.listTR();
									int aux4 = keyboard.nextInt();
									Transaction tr4 = grafo.findTR(aux4);
								
									//verificando se a operacao eh possivel nessa transacao
									if (tr4.getEstadoAtual() == estados.ATIVA || tr4.getEstadoAtual() == estados.EFETIVACAO) {
										grafo.mudarEstado(tr4, estados.CANCELAMENTO);
										System.out.println("Opera��o realizada com sucesso.");
									} else
										System.out.println("Esta operacao nao pode ser efetuada no momento.");
									break;
								
								case 4:		//tr commit
									//selecionando uma transacao para a operacao
									System.out.println("Selecione uma transacao:");
									grafo.listTR();
									int aux5 = keyboard.nextInt();
									Transaction tr5 = grafo.findTR(aux5);
								
									//verificando se a operacao eh possivel nessa transacao
									if (tr5.getEstadoAtual() == estados.EFETIVACAO) {
										grafo.mudarEstado(tr5, estados.EFETIVADA);
										System.out.println("Opera��o realizada com sucesso.");
									} else
										System.out.println("Esta operacao nao pode ser efetuada no momento.");
									break;
								
								case 5:		//tr finish
									//selecionando uma transacao para a operacao
									System.out.println("Selecione uma transacao:");
									grafo.listTR();
									int aux6 = keyboard.nextInt();
									Transaction tr6 = grafo.findTR(aux6);
								
									//verificando se a operacao eh possivel nessa transacao
									if (tr6.getEstadoAtual() == estados.EFETIVADA || tr6.getEstadoAtual() == estados.CANCELAMENTO) {
										grafo.mudarEstado(tr6, estados.FINALIZADA);
										System.out.println("Opera��o realizada com sucesso.");
									} else
										System.out.println("Esta operacao nao pode ser efetuada no momento.");
									break;
								
								case 6:	//voltar ao menu principal
									break;
								
								default:
									System.out.println("Op��o Inv�lida.");
							}
					}while (opt2 != 6);
				
				case 3:			//consultar transacao
		
					System.out.println("Selecione uma transacao:");
					grafo.listTR();
					int aux_search = keyboard.nextInt();
					Transaction tr_search = grafo.findTR(aux_search);
					System.out.println(tr_search.getEstadoAtual()+"\n");
					break;
				
				case 4:		//consultar grafo
					grafo.consultarEstados();
					
				default:
					System.out.println("Op��o Inv�lida.");
			
			}

		}while (opt!=4);
	}
}