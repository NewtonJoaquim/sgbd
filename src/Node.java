import java.util.ArrayList;

public class Node {
	//representa um possivel estado no grafo
	
	//Atributos
	public ArrayList<Transaction> tr = new ArrayList<Transaction>();
	public estados estadoNode;
		
	//Construtor
	public Node (estados estado) {
		estadoNode = estado;
	}

	//Getters & Setters
	public estados getTREstado() {
		return estadoNode;
	}
	
	//metodos
	public void addTR (Transaction tr) {
		this.tr.add(tr);
		tr.estadoAtual = estadoNode;
	}
	
	public void rmvTR (Transaction tr) {
		this.tr.remove(tr);
	}
	
	public boolean contains (Transaction tr2) {
		if (tr.contains(tr2)){
			return true;
		} else
			return false;
	}
	
}