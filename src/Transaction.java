
public class Transaction {
	
	//Possiveis estados de uma transacao:
	
	
	estados estadoAtual;
	protected static int trCount;
	protected int trID;		//identificador da transacao,ja que podemos ter multiplas
	
	//Construtor
	public Transaction () {
		estadoAtual = estados.INICIADA;
		trID = trCount++;	//aumenta em +1 o ID de cada nova transacao criada
	}
	
	public Transaction (int count) {
		estadoAtual = estados.INICIADA;
		trID = count;	//aumenta em +1 o ID de cada nova transacao criada
	}

	//Getters & Setters
	public estados getEstadoAtual() {
		return estadoAtual;
	}

	public void setEstadoAtual(estados estadoAtual) {
		this.estadoAtual = estadoAtual;
	}

	public static int getTrCount() {
		return trCount;
	}

	public int getTrID() {
		return trID;
	}

	
}